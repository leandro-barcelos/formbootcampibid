﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Formulario.aspx.cs" Inherits="FormBootcampIBID.Formulario" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="StyleSheet1.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .auto-style1 {
            font-family: Arial;
            font-style: normal;
            font-variant: normal;
            text-align: left;
            height: 23px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="formulario">
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: center"><h1>Formulário de Disponibilidade</h1></td>
                </tr>
                <tr>
                    <td class="indicador">Selecione os dias da semana:</td>
                </tr>
                <tr><td></td></tr>
                <tr>
                    <td style="align-content: center">
                        <asp:CheckBoxList ID="CheckBoxList1" runat="server" CssClass="cb_list" OnSelectedIndexChanged="Checklist_update" AutoPostBack="true">
                            <asp:ListItem>Domingo</asp:ListItem>
                            <asp:ListItem>Segunda-feira</asp:ListItem>
                            <asp:ListItem>Terça-feira</asp:ListItem>
                            <asp:ListItem>Quarta-feira</asp:ListItem>
                            <asp:ListItem>Quinta-feira</asp:ListItem>
                            <asp:ListItem>Sexta-feira</asp:ListItem>
                            <asp:ListItem>Sábado</asp:ListItem>
                        </asp:CheckBoxList>
                    </td>
                </tr>
                
                <tr><td></td></tr>
                <tr>
                    <td class="indicador" style="height:70px; vertical-align: top "><strong>Selecionados:</strong> <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
