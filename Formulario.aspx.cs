﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FormBootcampIBID
{
    public partial class Formulario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Checklist_update(object sender, EventArgs e)
        {
            string selecionados = "";

            foreach (ListItem item in CheckBoxList1.Items)
            {
                if (item.Selected)
                {
                    if (selecionados == "")
                    {
                        selecionados = item.Value;
                    }
                    else
                    {
                        selecionados += ", " + item.Value;
                    }
                    
                }
            }

            Label1.Text = selecionados;
            if (selecionados != "")
            {
                Label1.Visible = true;
            } else
            {
                Label1.Visible = false;
            }
        }
    }
}